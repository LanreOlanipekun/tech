import React, { Component } from 'react';
import '../assets/css/products.css'
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    Button,
    Container,
    Modal,
    ModalHeader,
    ModalBody,
    Form,
    FormGroup,
    Input,
    Label,
    Row,
    Col,
    Spinner,
    Alert
} from 'reactstrap';
import { connect } from 'react-redux';
import {addProduct} from '../actions/productActions';
import { withRouter} from 'react-router';

class TopNav extends Component {
    state = {
        isOpen: false,
        modal: false,
        fileName:null,
        file:null,
        color:null,
        name:null,
        description: null,
        price: null,
        category:null,
        errMsg:null,
        loading: false
    };
    
    toggle = () => {
        this.setState({
            isOpen: !this.state.isOpen
        });
        
    }
    toggleModal = () => {
        this.setState(prevState => ({
            modal: !prevState.modal
        }));
    }
    color = (e) => {
        this.setState({
            color: e.target.value
        })
    }
    selectFile = (e) => {
        this.setState({
            fileName: e.target.files[0].name,
            file: e.target.files[0]
        })
        setTimeout(() => {
           localStorage.setItem('image', this.state.file) 
        }, 0);
    }
    name = (e) => {
        this.setState({
            name: e.target.value
        })
    }
    desc = (e) => {
        this.setState({
            description: e.target.value
        })
    }
    price = (e) => {
        this.setState({
            price: e.target.value
        })
    }
    category = (e) => {
        this.setState({
            category: e.target.value
        })
    }
    submit = (e) => {
        e.preventDefault()
        if (!this.state.name || !this.state.description || !this.state.price || !this.state.category || !this.state.file || !this.state.color) {
            this.setState({
                errMsg:'All fields are required'
            })
        } else if (this.state.file.size > 2097152) {
            this.setState({
                msg: 'Please select image that is not more than 2mb'
            })
        } else if ((this.state.file.type === 'image/jpeg' || this.state.file.type === 'image/png') && this.state.file.size <= 2097152) {
            this.setState({
                loading: true
            })
            const formData = new FormData()
            formData.append('image', this.state.file)
            formData.append('name', this.state.name)
            formData.append('description', this.state.description)
            formData.append('price', this.state.price)
            formData.append('category', this.state.category)
            formData.append('color', this.state.color)
            this.props.addProduct(formData)
            .then(resp => {
                this.setState({
                    loading:false,
                    
                })
                this.props.history.push('/preview/'+resp._id)
            })
            .catch(err => {
                this.setState({
                    loading:false,
                    errMsg:'Can not add product at the moment'
                })
            })
        } else {
            this.setState({
                msg:'Please select an image in jpeg or png format'
            })
        }
    }
    product = ()=>{
        this.props.history.push('/')
    }

    render() {
        return (
            <div className="Products">
                <div>
                    <Navbar color="light" light expand="md">
                        <Container>
                            <NavbarBrand href="/" className="ml-2">{this.props.title}</NavbarBrand>
                            <NavbarToggler onClick={this.toggle} />
                            <Collapse isOpen={this.state.isOpen} navbar>
                                <Nav className="ml-auto" navbar>

                                    <NavItem>
                                        {this.props.page === 'product' ? <Button color="primary" onClick={this.toggleModal} >Add product</Button>:<Button color="primary" onClick={this.product} >products</Button>}
                                    </NavItem>

                                </Nav>
                            </Collapse>
                        </Container>
                    </Navbar>
                </div>


             {/* add product modal */}
             <div className="addProduct">
                    <Modal isOpen={this.state.modal} toggle={this.toggleModal} className={this.props.className}>
                        <ModalHeader toggle={this.toggleModal}>Add new product</ModalHeader>
                        <ModalBody>
                            <Form onSubmit={this.submit}>
                                {this.state.errMsg ? <Alert color="danger" className="text-center">{this.state.errMsg}</Alert>:null}
                                <Row form>
                                    <Col md={6}>
                                        <FormGroup>
                                            <Label for="name">Name</Label>
                                            <Input type="text" name="name" id="name" placeholder="Product name" onChange={this.name} />
                                        </FormGroup>
                                    </Col>
                                    <Col md={6}>
                                        <FormGroup>
                                            <Label for="price">Price</Label>
                                            <Input type="number" name="price" id="price" placeholder="Price" onChange={this.price} />
                                        </FormGroup>
                                    </Col>
                                </Row>
                                <FormGroup>
                                    <Label for="desc">Description</Label>
                                    <Input type="textarea" name="desc" id="desc" onChange={this.desc} />
                                </FormGroup>
                                <Row form>
                                    <Col md={6}>
                                        <FormGroup>
                                            <Label for="category">Category</Label>
                                            <Input type="select" name="category" id="category" onChange={this.category} >
                                                <option value="no data">Select category</option>
                                                <option value="Shoes">Shoes</option>
                                                <option value="Bags" >Bags</option>
                                                <option value="Tops" >Tops</option>
                                                <option value="Trousers" >Trousers</option>
                                                <option value = "Accessories">Accessories</option>
                                                <option value="Sun glasses" >Sun glasses</option>
                                            </Input>
                                        </FormGroup>
                                    </Col>
                                    <Col md={6}>
                                        <FormGroup>
                                            <Label for="price">Select Color</Label>
                                            <Input type="color" name="price" id="price" placeholder="Price" onChange={this.color} />
                                        </FormGroup>
                                    </Col>
                                </Row>
                                <label>Select picture</label>
                                <div className="input-group">
                                    <div className="custom-file">
                                        <input onChange={this.selectFile} type="file" className="custom-file-input" id="inputGroupFile01" />
                                        <label className="custom-file-label">{this.state.fileName}</label>
                                    </div>
                                </div>

                                <div className="confirm">
                                    <Button block color="primary" type="submit">{this.state.loading ? <Spinner color="secondary" />:'Add product'}</Button>
                                </div>
                            </Form>
                        </ModalBody>

                    </Modal>
                </div>
            </div>
        );
    }
}
const mapStateToProps = state => ({
    products: state.products

})
export default connect(mapStateToProps, {addProduct})(withRouter(TopNav));
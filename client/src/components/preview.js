import React, { Component } from 'react';
import '../assets/css/preview.css';
import {
    Row,
    Col,
    Card,
    FormGroup,
    Label,
    Input,
    Form,
    InputGroup,
    InputGroupAddon,
    InputGroupText
} from 'reactstrap';
import TopNav from './topNav';
import wish from '../assets/icons/wish.png';
import cart from '../assets/icons/cart.png';
import upload from '../assets/icons/upload.png';
import arrowRight from '../assets/icons/arrow-right.png';
import 'froala-editor/js/froala_editor.pkgd.min.js';
import 'froala-editor/css/froala_style.min.css';
import 'froala-editor/css/froala_editor.pkgd.min.css';
import FroalaEditor from 'react-froala-wysiwyg';
import { connect } from 'react-redux';
import {getProduct} from '../actions/productActions'

class Preview extends Component {
    _isMounted = false
    state = {
        editor: new FroalaEditor('#example'),
        loading: false
    }
    config = {
        placeholderText: 'Write here...',
        charCounterCount: false
    }
    componentDidMount () {
        this._isMounted = true
        if (this._isMounted) {
            this.setState({
                loading: true
            })
        }
        this.props.getProduct(this.props.match.params.id)
        .then(resp => {
            if (this._isMounted) {
                this.setState({
                    loading: false
                })
            }
        })
        .catch(err => {
            if (this._isMounted) {
                this.setState({
                    loading: false
                })
            }
        })

    }
    componentWillUnmount() {
        this._isMounted = false
    }
    handleModelChange = (e) => {
        console.log(e)
    }
    render() {
        return (
            <div>
                <div>
                    <TopNav title={'Preview'} page={'preview'}/>
                </div>
                {!this.state.isLoading ? <div className="Preview">
                    {this.props.products.singleProduct !== null ? <div>
                    <Row className="review">
                        <Col sm={6} md={6} lg={6} xl={3} style={{ padding: '0' }}>
                            <img src={'/uploads/'+this.props.products.singleProduct.image} alt="" />
                        </Col>
                        <Col sm={6} md={6} lg={6} xl={3} style={{ padding: '0' }}>
                            <h4>{this.props.products.singleProduct.name.toUpperCase()}</h4>
                            <p className="cat">{this.props.products.singleProduct.category}</p>
                            <span className="fa fa-star checked"></span>
                            <span className="fa fa-star checked"></span>
                            <span className="fa fa-star checked"></span>
                            <span className="fa fa-star"></span>
                            <span className="fa fa-star"></span>
                            <p className="desc">{this.props.products.singleProduct.description}</p>
                            <Row>
                                <Col xs={6} sm={6}>
                                    <div style={{ width: '20px', height: '20px', background: this.props.products.singleProduct.color, marginTop: '7px' }}></div>
                                </Col>
                                <Col xs={2} sm={2}>
                                    <div style={{ border: 'solid', borderRadius: '100%', borderColor: '#F09C32', borderWidth: '1px', background: '#F09C32', width: '35px', height: '35px' }} >
                                        <img src={wish} alt="" style={{ width: '35px', height: '35px', paddingLeft: '4px', paddingRight: '6px', paddingBottom: '6px', paddingTop: '6px' }} />
                                    </div>

                                </Col>
                                <Col xs={2} sm={3}>
                                    <div style={{ border: 'solid', borderRadius: '100%', borderColor: '#F09C32', borderWidth: '1px', background: '#F09C32', width: '35px', height: '35px' }} >
                                        <img src={cart} alt="" style={{ width: '35px', height: '35px', paddingLeft: '7px', paddingRight: '7px', paddingBottom: '10px', paddingTop: '10px' }} />
                                    </div>
                                </Col>
                            </Row>
                            <Row>
                                <Col sm={12}>
                                    <h2>₦ {this.props.products.singleProduct.price.toLocaleString()}</h2>
                                </Col>
                            </Row>
                        </Col>


                        <Col sm={12} md={12} lg={12} xl={6} className="edit" style={{ paddingRight: '0',paddingLeft:'0' }}>
                            <Card>
                                <Row>
                                    <Col sm={12} style={{ paddingRight: '0' }} className="text-center top">
                                        <h6>EDIT DIGITAL PRODUCT</h6>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={2} className="title text-center">
                                        <p>Item</p>
                                    </Col>
                                </Row>
                                <Row className="update">
                                    <Col sm={4} className="text-center">
                                        <FormGroup>
                                            <div className="wrapper">
                                                <div className="fakeuploadbutton">
                                                    <img src={upload} alt="" style={{ width: '35px', height: '35px' }} />
                                                    <p>Upload image</p>
                                                </div>
                                                <Input type="file" name="file" id="file" />
                                            </div>
                                        </FormGroup>
                                    </Col>
                                    <Col sm={8}>
                                        <Form>
                                            <FormGroup className="up-name">
                                                <Input type="text" name="name" id="name" placeholder="Update name" />
                                            </FormGroup>

                                            <FormGroup className="addOn">
                                                <Label>Pricing</Label>
                                                <InputGroup>
                                                    <Input placeholder="₦0.00" />
                                                    <InputGroupAddon addonType="append">
                                                        <InputGroupText>EDIT &nbsp;<img src={arrowRight} alt="" style={{ width: '15px', height: '15px' }} /></InputGroupText>
                                                    </InputGroupAddon>
                                                </InputGroup>
                                            </FormGroup>

                                            <FroalaEditor
                                                tag='textarea'
                                                config={this.config}
                                                onModelChange={this.handleModelChange}
                                            />
                                        </Form>
                                    </Col>
                                </Row>

                                <Row className="buttons">
                                    <Col sm={3}></Col>
                                    <Col sm={3} className="text-center update" style={{paddingRight:'0', paddingLeft:'0', cursor:'pointer'}}>
                                        <div className="but-bord">
                                        <p style={{marginBottom:'8px', marginTop:'8px'}}>UPDATE</p>
                                        </div>
                                        
                                    </Col>
                                    <Col sm={3} className="text-center update" style={{paddingRight:'0', paddingLeft:'0', cursor:'pointer'}}>
                                        <div className="but-bor">
                                        <p style={{marginBottom:'8px', marginTop:'8px'}}>CONTINUE</p>
                                        </div>
                                    </Col>
                                    <Col sm={3}>
                                    </Col>
                                </Row>
                            </Card>
                        </Col>
                    </Row>
                </div>:null}
                </div>:null}
            </div>
        );
    }
}
const mapStateToProps = state => ({
    products: state.products

})
export default connect(mapStateToProps, {getProduct})(Preview);
import {
    GET_PRODUCTS,
    ADD_PRODUCT,
    GET_A_PRODUCT,
    CLEAR_SINGLE
} from '../actions/types';

const initialState = {
    products: [],
    singleProduct: null
};

export default function (state = initialState, action) {
    switch (action.type) {
        case GET_PRODUCTS:
            return {
                ...state,
                products: action.payload
            }
        case ADD_PRODUCT:
            return {
                ...state,
                products: [action.payload, ...state.products]
            }
        case GET_A_PRODUCT:
            return {
                ...state,
                singleProduct: action.payload
            }
        case CLEAR_SINGLE:
            return {
                ...state,
                singleProduct:null
            }
        default:
            return state
    }
}
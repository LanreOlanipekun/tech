export const GET_PRODUCTS = 'GET_PRODUCTS';
export const ADD_PRODUCT = 'ADD_PRODUCT';
export const GET_A_PRODUCT = 'GET_A_PRODUCT';
export const CLEAR_SINGLE = 'CLEAR_SINGLE';

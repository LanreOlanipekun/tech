const Products = require('../models/products');
const mongoose = require('mongoose');

// get all products
exports.get_all_products = (req, res, next) => {
    Products.find().sort({Date: -1})
        .select('_id name price')
        .then(product => {
            return res.status(200).json({
                status: 'success',
                data: product
            })
        })
        .catch(err => {
            return res.status(500).json({
                status: 'error',
                error: err
            })
        })
}


// get single product
exports.get_single_product = (req, res, next) => {
    Products.findById(req.params.id)
        .then(product => {
            if (product) {
                return res.status(200).json({
                    status: 'success',
                    data: product
                })
            } else {
                return res.status(404).json({
                    status: 'error',
                    message: 'Product with id not found'
                })
            }
        })
}


// create product
exports.create_product = (req, res, next) => {
    const product = new Products({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        description: req.body.description,
        price: req.body.price,
        category: req.body.category,
        image: req.file.filename,
        color: req.body.color
    })
    product.save()
        .then(product => {
            return res.status(201).json({
                status: 'success',
                product
            })
        })
        .catch(err => {
            return res.status(500).json({
                status: 'error',
                error: err
            })
        })
}